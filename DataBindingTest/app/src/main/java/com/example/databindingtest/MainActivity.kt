package com.example.databindingtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.example.databindingtest.adapter.StudentAdapter

import com.example.databindingtest.databinding.ActivityMainBinding
import com.example.databindingtest.model.Student

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var listStudent : List<Student>
    private lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        init()
        setUpRecyclerView()

    }

    private fun setUpRecyclerView() {
        var listStudentAdapter = StudentAdapter(listStudent)
        var layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = listStudentAdapter
    }

    private fun init(){
        recyclerView = binding.recyclerView
        listStudent = listOf(
            Student("A","22"),
            Student("B","22"),
            Student("C","22"),
            Student("D","22"),
            Student("E","22"),
        )
    }
}