package com.example.databindingtest.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.databindingtest.databinding.ItemBinding
import com.example.databindingtest.model.Student

class StudentAdapter(var list : List<Student>) : RecyclerView.Adapter<StudentAdapter.StudentViewHolder>() {
    private var listStudent : List<Student> = list
    inner class StudentViewHolder(binding : ItemBinding) : RecyclerView.ViewHolder(binding.root){
        var tvName : TextView
        var tvAge : TextView
        init {
            tvName = binding.tvName
            tvAge = binding.tvAge
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        var inflate = LayoutInflater.from(parent.context)
        var itemBinding = ItemBinding.inflate(inflate,parent,false)
        return StudentViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        var student = list.get(position)
        holder.tvName.text = student.name
        holder.tvAge.text = student.age

    }

    override fun getItemCount(): Int {
       return listStudent.size
    }
}